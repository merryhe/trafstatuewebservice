/**
 * 
 */
package org.auxing.logic;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * java后台弹出框设置
 * 
 * @author gaosm
 *
 */
public class PopAlert extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 未登录弹出框
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public static void loginAlert(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		out.print("<script>alert('您还没有登录，请登录...'); </script>");
		out.flush();
		out.close();
	}
	
	/**
	 * 更新失败弹出框
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public static void updAlert(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		out.print("<script>alert('更新失败,请检查数据...'); </script>");
		out.flush();
		out.close();
	}
	
	/**
	 * 删除失败弹出框
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public static void delAlert(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		out.print("<script>alert('删除失败,请检查数据...'); </script>");
		out.flush();
		out.close();
	}
	

	/**
	 * 注册失败弹出框
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public static void regAlert(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		out.print("<script>alert('注册失败,请检查数据...'); </script>");
		out.flush();
		out.close();
	}
	

	/**
	 * 未检索到数据弹出框
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public static void checkAlert(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		out.print("<script>alert('此用户信息不存在！'); </script>");
		out.flush();
		out.close();
	}
	

	/**
	 * 未检索到数据弹出框
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public static void checkCustAlert(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		out.print("<script>alert('常用语信息不存在！'); </script>");
		out.flush();
		out.close();
	}
}
