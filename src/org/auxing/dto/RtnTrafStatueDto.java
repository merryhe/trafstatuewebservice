package org.auxing.dto;

import java.util.List;

/**
 * 管理员/手机发布的拥堵列表获取数据返回用dto
 * 
 * @author gaosm
 *
 */
public class RtnTrafStatueDto {
	
	// DB取得数据对象
	private List<TrafStatObj> rtnTrafStatObjList;
	
	// ERROR CODE
	private String errorId;

	/**
	 * @return the rtnTrafStatObjList
	 */
	public List<TrafStatObj> getRtnTrafStatObjList() {
		return rtnTrafStatObjList;
	}

	/**
	 * @param rtnTrafStatObjList the rtnTrafStatObjList to set
	 */
	public void setRtnTrafStatObjList(List<TrafStatObj> rtnTrafStatObjList) {
		this.rtnTrafStatObjList = rtnTrafStatObjList;
	}

	/**
	 * @return the errorId
	 */
	public String getErrorId() {
		return errorId;
	}

	/**
	 * @param errorId the errorId to set
	 */
	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}
	

}
