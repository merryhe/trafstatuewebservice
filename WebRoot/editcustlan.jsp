<%@ page language="java" pageEncoding="utf-8"%>
<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
		<!-- TemplateBeginEditable name="doctitle" -->
		<title>镇江移动视频维护平台</title>
		<!-- TemplateEndEditable -->
		<!-- TemplateBeginEditable name="head" -->
		<!-- TemplateEndEditable -->
		
		<script type="text/javascript">

			function user_upd()
			{
				<!-- 警员编号判断 -->
				if (document.form1.jybh.value=="" || document.form1.jybh.value==null) {
					alert("警员编号不能为空！");
					return false;
				}
				<!-- 用户姓名判断 -->
				if (document.form1.yhxm.value=="" || document.form1.yhxm.value==null) {
					alert("用户姓名不能为空！");
					return false;
				}
				<!-- 终端号码判断 -->
				if (document.form1.zdhm.value=="" || document.form1.zdhm.value==null) {
					alert("终端号码不能为空！");
					return false;
				}
				<!-- 登陆密码判断 -->
				if (document.form1.dlmm.value=="" || document.form1.dlmm.value==null) {
					alert("登陆密码不能为空！");
					return false;
				}
				<!-- 所属区域判断 -->
				if (document.form1.ssqy.value=="" || document.form1.ssqy.value==null) {
					alert("所属区域不能为空！");
					return false;
				}
				<!-- 申请成为管理员判断 -->
				if (document.form1.sqgly.value=="" || document.form1.sqgly.value==null) {
					sqgly = "1";
				} else {
					sqgly = "0";
				}
				window.location.href='UserServlet?method='+"upd"+'&jybh='+document.getElementById('jybh').value+'&yhxm='+document.getElementById('yhxm').value+'&dlmm='+document.getElementById('dlmm').value+
					'&ssqy='+document.getElementById('ssqy').value+'&zdhm='+document.getElementById('zdhm').value+'&sqgly='+document.getElementById('sqgly').value;
			}
		</script>
	</head>
	<link href="res/niceforms-default.css" rel="stylesheet" type="text/css" />
	<body bgcolor="#EEF2FB">
		<table width="100%" height="31" border="0" cellpadding="0"
			cellspacing="0" class="left_topbg" id="table2">
			<tr>
				<td height="31">
					<div>
						用户设置>>编辑用户
					</div>
				</td>
			</tr>
		</table>
		<div align="center">
			<form action="" method="post" id="form1" name="form1"
				class="niceform">
				<fieldset>
					<dl>
						<dt>
							<label for="jybh">
								警员编号
							</label>
						</dt>
						<dd>
							<input type="text" name="jybh" id="jybh" size="54" value="<%=request.getParameter("jybh") %>" />
						</dd>
					</dl>
					<dl>
						<dt>
							<label for="yhxm">
								用户姓名
							</label>
						</dt>
						<dd>
							<input type="text" name="yhxm" id="yhxm" size="54" value="<%=request.getParameter("yhxm") %>" />
						</dd>
					</dl>
					<dl>
						<dt>
							<label for="zdhm">
								终端号码
							</label>
						</dt>
						<dd>
							<input type="text" name="zdhm" id="zdhm" size="54" value="<%=request.getParameter("zdhm") %>" />
						</dd>
					</dl>
					<dl>
						<dt>
							<label for="dlmm">
								登陆密码
							</label>
						</dt>
						<dd>
							<input type="text" name="dlmm" id="dlmm" size="54" value="<%=request.getParameter("dlmm") %>" />
						</dd>
					</dl>
					<dl>
						<dt>
							<label for="ssqy">
								所属区域
							</label>
						</dt>
						<dd>
							<input type="text" name="ssqy" id="ssqy" size="54" value="<%=request.getParameter("ssqy") %>" />
						</dd>
					</dl>
					<dl>
						<dt>
							<label for="sqgly">
								申请成为管理员
							</label>
						</dt>
						<dd>
							<input type="text" name="sqgly" id="sqgly" size="54" value="<%=request.getParameter("sqgly") %>" />
						</dd>
					</dl>
					<dl class="userUpd">
						<input type="button" onclick="user_upd();" name="userUpd"
							id="userUpd" value="提    交" />
					</dl>
				</fieldset>
			</form>
		</div>
		</div>
		</div>
	</body>
</html>
