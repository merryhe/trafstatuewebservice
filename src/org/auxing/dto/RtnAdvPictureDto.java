package org.auxing.dto;

/**
 * 广告图片获取数据返回用
 * 
 * @author gaosm
 *
 */
public class RtnAdvPictureDto {

	// 广告图片存放地址的数组
	private String advAddress;
	
	/**
	 * @return the advAddress
	 */
	public String getAdvAddress() {
		return advAddress;
	}

	/**
	 * @param advAddress the advAddress to set
	 */
	public void setAdvAddress(String advAddress) {
		this.advAddress = advAddress;
	}

	/**
	 * @return the errorId
	 */
	public String getErrorId() {
		return errorId;
	}

	/**
	 * @param errorId the errorId to set
	 */
	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}

	// ERROR CODE
	private String errorId;
}
