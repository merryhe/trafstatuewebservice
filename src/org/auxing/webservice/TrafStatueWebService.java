package org.auxing.webservice;

import javax.jws.soap.SOAPBinding;

import org.auxing.service.TrafStatueService;

/**
 * 《智行镇江》数据接口
 * 
 * @author Administrator
 * 
 */
public class TrafStatueWebService {
	
	private TrafStatueService trafStatueService = new TrafStatueService();

	/**
	 * 管理员发布的拥堵列表获取数据的接口
	 * 
	 * @param jsonstr
	 *            json格式的字符串
	 * @return json格式的字符串
	 */
	@SOAPBinding
	public String getTrafStatue(String jsonstr) {
		return trafStatueService.getTrafStatue(jsonstr);
	}

//	/**
//	 * 手机用户发布的拥堵列表获取数据的接口
//	 * 
//	 * @return json格式的字符串
//	 */
//	@SOAPBinding
//	public String getReportInfo() {
//		return trafStatueService.getReportInfo();
//	}

	/**
	 * 预播放图片获取的接口
	 * 
	 * @param addressID
	 *            具体地址
	 * @return json格式的字符串
	 */
	@SOAPBinding
	public String getPrePicture(String addressID) {
		return trafStatueService.getPrePicture(addressID);
	}

	/**
	 * 广告图片获取的接口
	 * 
	 * @return json格式的字符串
	 */
	@SOAPBinding
	public String getAdvPicture() {
		return trafStatueService.getAdvPicture();
	}

	/**
	 * 路况资讯获取的接口
	 * 
	 * @return json格式的字符串
	 */
	@SOAPBinding
	public String getPathInfo() {
		return trafStatueService.getPathInfo();
	}

	/**
	 * 路况上报的接口
	 * 
	 * @param jsonstr
	 *            json格式的字符串
	 * @return json格式的字符串
	 */
	@SOAPBinding
	public String setReportInfo(String jsonstr) {
		return trafStatueService.setReportInfo(jsonstr);
	}
	
	/**
	 * 图片上传的接口
	 * 
	 * @param jsonstr
	 *            json格式的字符串
	 * @return json格式的字符串
	 */
	@SOAPBinding
	public String setReportImage(String jsonstr) {
		return trafStatueService.setReportImage(jsonstr);
	}
}
