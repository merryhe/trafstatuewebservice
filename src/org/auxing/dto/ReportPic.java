/**
 * 
 */
package org.auxing.dto;

/**
 * 图片上传信息
 * 
 * @author gaosm
 *
 */
public class ReportPic {
	
	// 文件储存地址
	private String pic_name;
	
	// 图片资源
	private String image;

	/**
	 * @return the pic_name
	 */
	public String getPic_name() {
		return pic_name;
	}

	/**
	 * @param picName the pic_name to set
	 */
	public void setPic_name(String picName) {
		pic_name = picName;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

}
