/**
 * 
 */
package org.auxing.logic;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.auxing.dao.TrafStatueDao;
import org.auxing.dto.CustLanDto;

/**
 * 常用语配置用servlet
 * 
 * @author gaosm
 * 
 */
public class CustDesServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// mybatis接口
	private static TrafStatueDao trafStatueDao;

	private RequestDispatcher dispatcher = null;

	// SqlSession定义
	private static SqlSession sqlSession;
	static SqlSessionFactory sqlSessionFactory = null;
	static {
		sqlSessionFactory = DbInit.getSqlSessionFactory();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		String method = request.getParameter("method");
		if ("add".equals(method)) {
			custLanServletIns(request, response);
		} else if ("del".equals(method)) {
			custLanServletDel(request, response);
		} else if ("upd".equals(method)) {
			custLanServletUpd(request, response);
		} else if ("sel".equals(method)) {
			custLanServletSel(request, response);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occure
	 */
	public void init() throws ServletException {
		// Put your code here
	}

	/**
	 * 常用语检索
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public List<CustLanDto> custLanServletSel(HttpServletRequest request,
			HttpServletResponse response) {

		System.out.println("=========sel==========");
		List<CustLanDto> custLanDtoList = null;
		try {
			// 重新建立一个新的session
			sqlSession = sqlSessionFactory.openSession();
			// 配置文件中的namespace
			trafStatueDao = (TrafStatueDao) sqlSession
					.getMapper(TrafStatueDao.class);

			custLanDtoList = trafStatueDao.selCustLan();
			System.out.println(custLanDtoList.size());

			if (custLanDtoList.size() > 0) {
				dispatcher = request.getRequestDispatcher("/custlan.jsp");
				dispatcher.forward(request, response);
			} else {
				PopAlert.checkCustAlert(request, response);
				dispatcher = request.getRequestDispatcher("/custlan.jsp");
				dispatcher.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return custLanDtoList;
	}

	/**
	 * 常用语更新
	 * 
	 * @param request
	 * @param response
	 */
	public void custLanServletUpd(HttpServletRequest request,
			HttpServletResponse response) {

		try {
			CustLanDto inCustLanDto = new CustLanDto();
			inCustLanDto.setCustDescribe(request.getParameter("custDescribe"));
			inCustLanDto.setInformation(request.getParameter("information"));

			// 重新建立一个新的session
			sqlSession = sqlSessionFactory.openSession();
			// 配置文件中的namespace
			trafStatueDao = (TrafStatueDao) sqlSession
					.getMapper(TrafStatueDao.class);

			trafStatueDao.updCustLan(inCustLanDto);

			dispatcher = request.getRequestDispatcher("/login.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 常用语新增
	 * 
	 * @param request
	 * @param response
	 */
	public void custLanServletIns(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			// 常用语入力信息取得
			CustLanDto inCustLanDto = new CustLanDto();
			inCustLanDto.setCustDescribe(request.getParameter("custDescribe"));
			inCustLanDto.setInformation(request.getParameter("information"));

			// 重新建立一个新的session
			sqlSession = sqlSessionFactory.openSession();
			// 配置文件中的namespace
			trafStatueDao = (TrafStatueDao) sqlSession
					.getMapper(TrafStatueDao.class);

			trafStatueDao.insCustLan(inCustLanDto);

			dispatcher = request.getRequestDispatcher("/login.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 常用语删除
	 * 
	 * @param request
	 * @param response
	 */
	public void custLanServletDel(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			// 常用语入力信息取得
			List<CustLanDto> inCustLanDtoList = new ArrayList<CustLanDto>();
			CustLanDto inCustLanDto = new CustLanDto(); // TODO
			inCustLanDto.setCustDescribe(request.getParameter("custDescribe"));
			inCustLanDto.setInformation(request.getParameter("information"));
			inCustLanDtoList.add(inCustLanDto);

			// 重新建立一个新的session
			sqlSession = sqlSessionFactory.openSession();
			// 配置文件中的namespace
			trafStatueDao = (TrafStatueDao) sqlSession
					.getMapper(TrafStatueDao.class);

			trafStatueDao.delCustLan(inCustLanDtoList);

			dispatcher = request.getRequestDispatcher("/login.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
